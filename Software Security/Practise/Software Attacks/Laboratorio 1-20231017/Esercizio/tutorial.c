#include<stdio.h>
#include <stdlib.h>

void secret_fun() {
	printf("You won!\n");
}

void fun(int n) {
	int val;
	int my_lucky;
	char name[11];
	
	val = n;
	my_lucky = 78;
	
	printf("Insert your name: ");
	scanf("%s", name);
	
	if( val == my_lucky )
		printf("\nHey %s that's also my lucky number too!!\n", name);
	else printf("\nHey %s keep secret your lucky number %d !\n", name, val);
	
	return;
}

int main(){
	int value;
	
	printf("Welcome to the tutorial on x86 architectures!\n"); 
	printf("Please try to answer the questions on the slides by analyzing me through gdb.\n\n");
	
	printf("Insert you lucky number: ");
	scanf("%d", &value);
	
	fun(value);
	
	return 0;
}



