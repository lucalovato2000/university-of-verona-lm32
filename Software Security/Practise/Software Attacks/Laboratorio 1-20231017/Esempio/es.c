#include <stdio.h>
#include <string.h>

int second(int n) {
	char word[10];
	int result;
	
	printf("Please write a word: ");
	scanf("%s", word);
	
	result = n - strlen(word);
	
	return result;
}

void first() {
	int num;
	int result;

	printf("Please type a number: ");
	scanf("%d", &num);
	
	if(num % 2 == 0)
		printf("%d is even\n", num);
	else	printf("%d is odd\n", num);
	
	result = second(num);
	
	printf("result = %d\n", result);

	return;
}

int main() {
	printf("Hello world!\n");
	
	first();
	
	printf("Ending program... bye!\n");
	
	return 0;
}

