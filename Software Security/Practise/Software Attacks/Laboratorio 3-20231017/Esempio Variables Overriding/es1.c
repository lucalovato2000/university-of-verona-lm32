#include<stdio.h>
#include<string.h>

void function(char *str){
	int variable;
	char buffer[10];
	
	variable = 0;
	strcpy(buffer, str);
	
	if(variable == 0x30324343) {
		printf("You have changed the variable with the correct value!\n");
	}
	else { 
		printf("Try again, you got 0x%08x\n", variable);
	}
}

int main(int argc, char **argv){
	
	if (argc == 1){
		printf("Please specify an argument!\n");
		return 0;
	}
	
	function(argv[1]);
	
	return 0;
}
