#include<stdio.h>
#include<string.h>

void highSecurityFunction(){
	printf("You have executed a function with high security level!\n");
	
	return;
}

void lowSecurityFunction(){
	char buffer[10];
	
	printf("Enter some text:\n");
	scanf("%s", buffer);
	
	printf("Exiting...\n");
	
	return;
}

int main(int argc, char **argv){
	
	lowSecurityFunction();
	
	return 0;
}
