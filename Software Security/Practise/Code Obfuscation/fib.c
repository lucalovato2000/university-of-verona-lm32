// Uncomment this if you're on Darwin. "apple.h" is in the 
// Tigress distribution package.
// #include "apple.h"

#include<stdio.h>
#include<stdlib.h>

// Uncomment this for the JitDynamic transformation:
//#include "jitter-amd64.c"

int fib(int n) {
   int a = 1;
   int b = 1;
   int i;
   for (i = 3; i <= n; i++) {
      int c = a + b;
      a = b;
      b = c;
   };
   return b;
}

int main(int argc, char** argv) {
  if (argc != 2) {
     printf("Give one argument!\n");
     abort();
  };
  long n = strtol(argv[1],NULL,10);
  int f = fib(n);
  printf("fib(%li)=%i\n",n,f);
}
