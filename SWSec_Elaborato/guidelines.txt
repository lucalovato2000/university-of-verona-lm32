Per il progetto del corso potete:

- Approfondire qualche tematica di laboratorio contattando Marco Campion (e lasciando me in cc)
- Approfondire qualche argomento trattato a lezione
- Proporre nuove soluzioni / tecniche di protezione del software o di attacco
- Se avete idee sottoponetemele altrimenti cerchiamo un argomento insieme

Quando lo studio / approfondimento è stato svolto i risultati vengono raccolti in un report che mi
mandare prima dell’esame.

L’esame si svolge su appuntamento e consiste di;

- 20/25 minuti in cui mi presentate con il supporto di slides lo studio / progetto / nuova soluzione
- Domande sui temi trattati durante il corso

Nel caso di progetti di gruppo si dovranno concordare i task di ognuno.

Questo documento contiene dei possibili spunti su possibili approfondimenti per il progetto di
sicurezza del SW. Nel documento ci sono elencati alcuni argomenti con dei riferimenti ad alcuni
paper per meglio capire l’idea dell’approfondimento (in questi casi va comunque fatta la ricerca
dello stato dell’arte per aggiornare i contributi)
Per quanto riguarda la ricerca dello state dell’arte: cercate su google il nome della conferenza con
l’indicazione dell’anno 2023/2022/2021/2020 e cercate il programma o la lista degli accepted
papers, oppure andare su DBLP e cercate li la conferenza. Scorrete i titoli e date un’occhiata
all’abstract e identificate 2/3 paper inerenti l’argomento di vostro interesse e me li mandate e poi
valutiamo assieme quali aspetti approfondire. Quando si studiano i paper va sempre capito bene
quale miglioria è stata ottenuta e quali limiti ha l’approccio proposto.
Se avete problemi a scaricare i paper collegatevi in vpn all’uni che è abbonata a diverse case
editrici, oppure cercate il titolo del paper su googlescholar dove spesso è semplice recuperare
i .pdf

=============================================================================================================================

SW & HW code obfuscation: code obfuscation techniques are used also at the HW level (i.e. diversi ed instruction sets)

- Search the papers published in the last year in the main security conferences on this topic
- Critical analysis of the state of the art: How does obfuscation work in HW? What are the limits of HW obfuscation? What are the existing technologies.
- Study some of the main existing solutions - Propose potential improvements or novel solution
- PUF: how can we exploit them for software security / protection